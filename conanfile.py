from conans import ConanFile, CMake, tools
import os

class ChehConan(ConanFile):
    name = 'cheh'
    version = '0.1.0'
    license = 'MIT'
    author = 'Julien Bernard jbernard@obspm.fr'
    url = ''
    description = 'C++ log, error & alarm for COSMIC.'

    requires = [
        'fmt/7.0.3',
        'log4cplus/2.0.4@bincrafters/stable'
    ]

    options = {'shared':  [True, False],
               'fPIC':    [True, False],
               'test':    [True, False]}

    default_options = {'shared' : False,
                       'fPIC'   : True,
                       'test'   : False}

    settings = 'os', 'compiler', 'build_type', 'arch'
    generators = 'cmake'
    exports_sources = 'cmake/*', 'include/*', 'src/*', 'CMakeLists.txt'

    def requirements(self):
        if self.options.test:
            self.requires('gtest/1.8.1@bincrafters/stable')

    def build(self):
        cmake = CMake(self)
        cmake.definitions['cheh_build_test'] = self.options.test

        cmake.configure(source_folder='.')
        cmake.build()
        if self.options.test:
            cmake.test()

    def package(self):
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a" , dst="lib", keep_path=False)

        self.copy("*.h" , dst="include", src="include")

        self.copy("*" , dst="share/cmake/cheh", src="cmake")



    def package_info(self):
        self.cpp_info.libs = ['chehlog']
