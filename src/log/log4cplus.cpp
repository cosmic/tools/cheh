#include <cheh/log/log4cplus.h>

namespace cheh
{

namespace log
{
    Handle::Handle() {
        LogManager::init();
    }

    Handle::~Handle() {
        LogManager::shutdown();
    }


    std::mutex LogManager::access_mutex = {};
    // bool LogManager::instanced = false;
    std::unique_ptr<LogManager> LogManager::log_manager = {};

    LogManager::LogManager():
        initializer(), config(),
        main_appender_()
    {
        config.configure();
        log4cplus::Logger::getRoot().removeAllAppenders();

        reset_layout();
    }

    void LogManager::init() {
        std::lock_guard<std::mutex> guard(access_mutex);

        if ( ! log_manager )
            log_manager = std::make_unique<LogManager>();
        else
            throw std::logic_error("Log manager already created.");
    }

    void LogManager::shutdown() {
        std::lock_guard<std::mutex> guard(access_mutex);

        log_manager.reset();

        //TODO Considere to shutdown loggers here
        // log4cplus::Logger::shutdown();
    }

    LogManager& LogManager::instance() {
        if ( ! log_manager )
            throw std::logic_error("Log handler not created.");

        return *log_manager;
    }

    LogManager& LogManager::set_file_appender(const std::string& filename) {
        //TODO implement it.
        return *this;
    }
    Appender LogManager::main_appender() const {
        return main_appender_;
    }
    Appender LogManager::appender(std::size_t id) const {
        auto res = appenders_.find(id);
        return (res != appenders_.end() ? res->second : main_appender());
    }

    void LogManager::insert_appender(std::size_t id, Appender appender) {
        appenders_.emplace(id, appender);
    }

    std::string LogManager::default_layout() const {
        return "%-5p - %X{module} - %m%n";
    }
    void LogManager::reset_layout() {
        set_layout(default_layout());
    }
    void LogManager::set_layout(const std::string& pattern) {
        main_appender().appender()->setLayout(std::make_unique<log4cplus::PatternLayout>(pattern));
    }

    thread_local Logger<0> local_logger;

    const log4cplus::Logger& get_logger() {
        return local_logger.logger();
    }

} // namespace log


} // namespace cheh
