#include <cheh/log/appender.h>

#include <log4cplus/fileappender.h>
#include <log4cplus/consoleappender.h>

namespace cheh
{
    void set_properties(log4cplus::helpers::Properties& props, const pair_list& properties) {
        std::for_each(
            std::begin(properties), std::end(properties),
            [&props](auto pair) { props.setProperty(pair.first, pair.second); }
        );
    }

    log4cplus::SharedAppenderPtr make_console_appender() {
        log4cplus::helpers::Properties props;
        set_properties(props, {
            {"AsyncAppend"   , "true" },
            {"logToStdErr"   , "false"},
            {"ImmediateFlush", "true" }
        });

        return log4cplus::SharedAppenderPtr(
            new log4cplus::ConsoleAppender(props)
        );
    }

    log4cplus::SharedAppenderPtr make_file_appender(std::string filename) {
        log4cplus::helpers::Properties props;
        set_properties(props, {
            {"File"          , filename},
            {"AsyncAppend"   , "true"  },
            {"CreateDirs"    , "true"  },
            {"ImmediateFlush", "true"  }
        });

        return log4cplus::SharedAppenderPtr(
            new log4cplus::RollingFileAppender(props)
        );
    }
} // namespace cheh
