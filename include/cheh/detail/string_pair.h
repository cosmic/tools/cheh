#ifndef CHEH_DETAIL_STRING_PAIR_H
#define CHEH_DETAIL_STRING_PAIR_H

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ranges.h>

#include <string>


namespace cheh
{
    namespace detail {
        inline std::string to_string(const std::string & s) {
            return s;
        }

        inline std::string to_string(std::string && s) {
            return std::move(s);
        }

        template<typename T>
        std::string to_string(T && t) {
            return fmt::to_string(std::forward<T>(t));
        }
    }

    struct string_pair
    {
        std::string first, second;

        template<typename V>
        constexpr string_pair(const std::string & k, const V & v):
            first(k), second(detail::to_string(v))
        {}

        template<typename V>
        constexpr string_pair(std::string && k, const V & v):
            first(std::move(k)), second(detail::to_string(v))
        {}
    };

} // namespace cheh

template<>
struct fmt::formatter<cheh::string_pair>{
    template<typename ParseContext>
    constexpr auto parse(ParseContext& ctx) const{
        return ctx.begin();
    }

    template<typename FormatContext>
    constexpr auto format(cheh::string_pair const& mp, FormatContext& ctx) const{
        return fmt::format_to(ctx.out(), "{{{}:{}}}", mp.first, mp.second);
    }
};

namespace cheh
{
    inline std::string format(const std::initializer_list<string_pair> & list) {
        return fmt::format("{}", fmt::join(list, ","));
    }
} // namespace cheh


#endif //CHEH_DETAIL_STRING_PAIR_H