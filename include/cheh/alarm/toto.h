#ifndef CHEH_ALARM_TOTO_H
#define CHEH_ALARM_TOTO_H

#include <cheh/alarm/type.h>

namespace cheh
{
    void alert(alarm_type at);

    void alert_if(bool condition, alarm_type at);

}

#endif //CHEH_ALARM_TOTO_H