#ifndef CHEH_ALARM_TYPE_H
#define CHEH_ALARM_TYPE_H

namespace cheh
{

    enum class AlarmType
    {
        skippedFrame,
        clippedActuator,
        saturatedPupil,
        darkPupil
    };



}

#endif //CHEH_ALARM_TYPE_H