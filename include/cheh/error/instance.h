#ifndef CHEH_ERROR_ERROR_INSTANCE_H
#define CHEH_ERROR_ERROR_INSTANCE_H

#include <cheh/error/base.h>
#include <cheh/error/type.h>

namespace cheh
{

    struct TimeOutError : ErrorBase<TimeOutError>
    {
        using Base = ErrorBase<TimeOutError>;
        TimeOutError(std::string msg):
            Base(ErrorType::timeout, std::move(msg))
        {}
    };

}

#endif //CHEH_ERROR_ERROR_INSTANCE_H