#ifndef CHEH_ERROR_ERROR_TYPE_H
#define CHEH_ERROR_ERROR_TYPE_H

#include <string>

namespace cheh
{

    enum class ErrorType
    {
        timeout = 123
        //...
    };


    constexpr inline std::string get_error_msg(ErrorType et)
    {
        switch (et)
        {
            case ErrorType::timeout : return "timeout";
        }
    }
}

#endif //CHEH_ERROR_ERROR_TYPE_H