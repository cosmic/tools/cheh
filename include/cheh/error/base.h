#ifndef CHEH_ERROR_ERROR_BASE_H
#define CHEH_ERROR_ERROR_BASE_H

#include <cheh/error/type.h>
#include <cheh/log.h>

#include <string>

namespace cheh
{

    template<typename Derived>
    struct ErrorBase
    {
        ErrorType error_type_;
        std::string msg_;

        ErrorBase(ErrorType error_type, std::string msg):
            error_type_(error_type), msg_(std::move(msg))
        {}

        Derived& self() noexcept { return static_cast<Derived&>(*this); }
        const Derived& self() const { return static_cast<const Derived&>(*this); }

        void log() const
        {
            LOG(CHEH_ERROR, self().what());
        }

        void log(Logger logger) const
        {
            LLOG(CHEH_ERROR, logger, self().what());
        }

        const char * what() const
        {
            return fmt.format("Error {} with reason : ", get_error_msg(error_type_), msg_);
        }
    };

}

#endif //CHEH_ERROR_ERROR_BASE_H