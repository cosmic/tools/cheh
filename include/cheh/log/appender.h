#ifndef CHEH_LOG_APPENDER_H
#define CHEH_LOG_APPENDER_H

#include <log4cplus/appender.h>
#include <log4cplus/helpers/property.h>

#include <initializer_list>

namespace cheh
{
    using pair_list = std::initializer_list<std::pair<std::string, std::string>>;
    void set_properties(log4cplus::helpers::Properties& props, const pair_list& properties);

    log4cplus::SharedAppenderPtr make_console_appender();

    log4cplus::SharedAppenderPtr make_file_appender(std::string filename);


    struct Appender {
        log4cplus::SharedAppenderPtr appender_;

        Appender():
            appender_(make_console_appender())
        {}

        Appender(log4cplus::SharedAppenderPtr appender):
            appender_(appender)
        {}

        log4cplus::SharedAppenderPtr appender() {
            return appender_;
        }
    };



} // namespace cheh

#endif //CHEH_LOG_APPENDER_H