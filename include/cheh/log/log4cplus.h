#ifndef CHEH_DETAIL_LOG_LOG4CPUS_H
#define CHEH_DETAIL_LOG_LOG4CPUS_H

#include <cheh/log/appender.h>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/initializer.h>
#include <log4cplus/helpers/pointer.h>
#include <log4cplus/consoleappender.h>

#include <fmt/core.h>
#include <fmt/ostream.h>

#include <thread>

namespace cheh
{

namespace log
{
    // Handle log scope. unique & local.
    // Needed to be attached to at least main scope (or more local).
    // Will init and destroy everythings.
    struct Handle;

    // Handle log configuration. unique & global.
    struct LogManager;

    // Handle a logger. Multiple & local.
    template<std::size_t ID>
    struct Logger;

    struct Handle
    {
        Handle();
        ~Handle();

        Handle(Handle &&) = default;
        Handle(const Handle &) = delete;

        Handle& operator=(Handle &&) = default;
        Handle& operator=(const Handle &) = delete;
    };

    struct LogManager
    {
        log4cplus::Initializer initializer;
        log4cplus::BasicConfigurator config;
        Appender main_appender_;
        std::map<std::size_t, Appender> appenders_;

        LogManager();
        ~LogManager() = default;

        static void init();
        static void shutdown();
        static LogManager& instance();

        LogManager& set_file_appender(const std::string& filename);
        Appender main_appender() const;
        Appender appender(std::size_t id) const;

        void insert_appender(std::size_t id, Appender appender);

        std::string default_layout() const;
        void reset_layout();
        void set_layout(const std::string&  pattern);

        private:

        static std::mutex access_mutex;

        static std::unique_ptr<LogManager> log_manager;
    };

    template<std::size_t ID>
    struct Logger
    {
        log4cplus::Logger logger_;

        Logger(): logger_(log4cplus::Logger::getInstance(
            fmt::format("{}", std::this_thread::get_id()))
        ) {
            // By default, log in main appender
            logger_.addAppender(LogManager::instance().appender(ID).appender());
        }

        ~Logger() = default;

        const log4cplus::Logger& logger() const {
            return logger_;
        }
    };

    const log4cplus::Logger& get_logger();
} // namespace log

}

#endif //CHEH_DETAIL_LOG_LOG4CPUS_H