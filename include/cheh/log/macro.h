#ifndef CHEH_DETAIL_LOG_MACRO_H
#define CHEH_DETAIL_LOG_MACRO_H

#include <cheh/detail/string_pair.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/mdc.h>

#include <cheh/log/log4cplus.h>

#define CHEH_LLOG_IMPL(LEVEL, LOGGER, CONTEXT, ...)                                     \
    do {                                                                                \
        log4cplus::getMDC ().put("module", CONTEXT);  \
        LEVEL(LOGGER, ::cheh::format({__VA_ARGS__}));                                   \
    } while(false)

#define CHEH_LOG(LEVEL, ...)                                                            \
    CHEH_LLOG_IMPL(LEVEL, ::cheh::log::get_logger(), "cheh", __VA_ARGS__)

#define CHEH_LLOG(LEVEL, LOGGER, ...)                                                   \
    CHEH_LLOG_IMPL(LEVEL, LOGGER, "cheh", __VA_ARGS__)

// Aliases
#define C_LOG CHEH_LOG
#define C_LLOG CHEH_LLOG

#define EMU_LOG(LEVEL, ...)                                               \
    CHEH_LLOG_IMPL(LEVEL, ::cheh::log::get_logger(), "emu", __VA_ARGS__)

#define EMU_LLOG(LEVEL, LOGGER, ...)                                      \
    CHEH_LLOG_IMPL(LEVEL, LOGGER, "emu", __VA_ARGS__)

// Complete macro definition
#define CHEH_DEBUG LOG4CPLUS_DEBUG_STR
#define CHEH_INFO  LOG4CPLUS_INFO_STR
#define CHEH_WARN  LOG4CPLUS_WARN_STR
#define CHEH_ERROR LOG4CPLUS_ERROR_STR
#define CHEH_FATAL LOG4CPLUS_FATAL_STR

// Short name definition
#define C_DEBUG  CHEH_DEBUG
#define C_INFO   CHEH_INFO
#define C_WARN   CHEH_WARN
#define C_ERROR  CHEH_ERROR
#define C_FATAL  CHEH_FATAL

#endif //CHEH_DETAIL_LOG_MACRO_H