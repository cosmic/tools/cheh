#ifndef CHEH_EXCEPTION_H
#define CHEH_EXCEPTION_H

#include <exception>
#include <type_traits>

namespace cheh
{

    namespace detail
    {
        template< class T >
        struct remove_cvref {
            typedef std::remove_cv_t<std::remove_reference_t<T>> type;
        };

        template< class T >
        using remove_cvref_t = typename remove_cvref<T>::type;
    }

    template<typename Error>
    struct Exception : std::exception
    {
        Error error_;

        Exception(Error error):
            error_(std::move(error))
        {}

        const char * what() override
        {
            return error_.what();
        }
    };

    template<typename Error>
    constexpr Exception<remove_cvref_t<Error>> make_exceptiom(Error && error)
    {
        return {std::forward<Error>(error)};
    }

    template<typename Error>
    constexpr void throw_error(Error && error)
    {
        throw make_exceptiom(std::forward<Error>(error));
    }


}

#endif //CHEH_EXCEPTION_H