# CHEH - COSMIC Hazard & Error Handling

CHEH is a library that propose utilities for handling problem in COSMIC framework.

## Requirement

This project need to be build :

- unix development environment (gcc, make, cmake).
- [conan](https://conan.io/) - C++ pakage manager.
- [fmt](https://fmt.dev) - an open-source formatting library.
- [log4cplus](https://sourceforge.net/projects/log4cplus/) - a simple to use logging API.

## Install

<!-- TODO -->

## Logging

