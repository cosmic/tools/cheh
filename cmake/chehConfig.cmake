

#[[
Generated custom log macro for a specific target.
- TARGET The target that will get access to the generated log macro.
- HEADER The header name with path that will be generated & will be included by target.
- SOURCE The source name with path that will be compiled by target.
- NAME The module name that will appear in log message.
- MACRO_PREFIX The prefix of macros that will be generated ({MACRO_PREFIX}_LOG & {MACRO_PREFIX}_LLOG).
- NAMESPACE namespace where functions will be generated. Can be nested namespaces.
]]
function(targed_configure_log)
    set(one_value_args HEADER SOURCE NAME MACRO_PREFIX NAMESPACE)
    list(POP_FRONT ARGN target)
    cmake_parse_arguments(LOG "" "${one_value_args}" "" ${ARGN} )

    if(NOT TARGET ${target})
        message(FATAL_ERROR "${target} is not a target")
    endif()

    # Convert into list.
    string(REPLACE "::" ";" NAMESPACE_LIST ${LOG_NAMESPACE})

    # Generated nested namespaces (Not available prior to C++ 17)
    foreach(NAMESPACE ${NAMESPACE_LIST})
        list(APPEND LOG_NAMESPACE_BEGIN "namespace ${NAMESPACE} {")
        list(APPEND LOG_NAMESPACE_END   "}"                       )
    endforeach()

    # Convert to string.
    list(JOIN LOG_NAMESPACE_BEGIN " " LOG_NAMESPACE_BEGIN)
    list(JOIN LOG_NAMESPACE_END   " " LOG_NAMESPACE_END  )

    # Generate header guard.
    string(TOUPPER ${LOG_HEADER} LOG_GUARD)
    string(REPLACE "/" "_" LOG_GUARD ${LOG_GUARD})
    string(REPLACE "." "_" LOG_GUARD ${LOG_GUARD})

    # Generated unique ID to store appender based on module name.
    string(MD5 LOG_HASH ${LOG_NAME})
    string(SUBSTRING ${LOG_HASH} 0 16 LOG_HASH)

    # Configure header and sources files
    configure_file(${CMAKE_CURRENT_FUNCTION_LIST_DIR}/log.h.in   generated/${LOG_HEADER} @ONLY)
    configure_file(${CMAKE_CURRENT_FUNCTION_LIST_DIR}/log.cpp.in generated/${LOG_SOURCE} @ONLY)

    # Adds source file and header location to target.
    target_sources(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/generated/${LOG_SOURCE})
    target_include_directories(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/generated)

endfunction(configure_log)
